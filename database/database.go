package database

import (
	"database/sql"
	"fmt"
	"os"
)

var Host = os.Getenv("HOST")
var User = os.Getenv("POSTGRES_USER")
var Password = os.Getenv("POSTGRES_PASSWORD")
var DBname = os.Getenv("POSTGRES_DB")
var Port = 5432

type DB struct {
	*sql.DB
}

func SetupDatabase() (*DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		Host, Port, User, Password, DBname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}
	return &DB{db}, nil
}
