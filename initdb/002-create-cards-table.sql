CREATE TABLE IF NOT EXISTS cards (
    id serial PRIMARY KEY,
    type varchar(256) NOT NULL,
    name varchar(256) NOT NULL UNIQUE,
    folder varchar(256),
    cardholder_name varchar(256) NOT NULL,
    brand varchar(256) NOT NULL,
    card_number varchar(256) NOT NULL UNIQUE,
    expiration_month int NOT NULL,
    expiration_year int NOT NULL,
    security_code varchar(4) NOT NULL UNIQUE,
    notes text
)