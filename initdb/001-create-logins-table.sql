CREATE TABLE IF NOT EXISTS logins (
    id serial PRIMARY KEY,
    type varchar(256) NOT NULL,
    name varchar(256) NOT NULL UNIQUE,
    folder varchar(256),
    username varchar(256) NOT NULL UNIQUE,
    password varchar(256) NOT NULL UNIQUE,
    uri varchar(256)[],
    notes text
)