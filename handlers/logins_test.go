package handlers

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"passward-backend/data"
	"passward-backend/database"
	"reflect"
	"strings"
	"testing"
)

func TestGetAllLoginsHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/logins", nil)
	if err != nil {
		t.Fatal(err)
	}
	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		DB:  *db,
		Log: *log.Default(),
	}
	s.GetAllLogins(tr, req)
	if status := tr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	res := []data.Login{}
	empty := []data.Login{}

	login := &data.Login{
		Id:       1,
		Type:     "Login",
		Name:     "Test Login",
		Folder:   "Logins",
		Username: "Username",
		Password: "Password",
		URI:      []string{"www.example.com"},
		Notes:    "This is a note.",
	}
	expected := append(empty, *login)
	unmarshalErr := json.Unmarshal(tr.Body.Bytes(), &res)
	if unmarshalErr != nil {
		t.Errorf(unmarshalErr.Error())
	}
	if len(expected) < len(res) || len(expected) > len(res) {
		t.Errorf("handler returned unexpected body, got %+v want %+v", len(res), len(expected))
	}
	for k, v := range res {
		if v.Type != expected[k].Type {
			t.Errorf("handler returned unexpected body: got %+v want %+v", tr.Body.String(), expected)
		}
		if v.Name != expected[k].Name {
			t.Errorf("handler returned unexpected body: got %+v want %+v", tr.Body.String(), expected)
		}
		if v.Folder != expected[k].Folder {
			t.Errorf("handler returned unexpected body: got %+v want %+v", tr.Body.String(), expected)
		}

	}

}

func TestGetSingleLoginHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/login/1", nil)
	if err != nil {
		t.Fatal(err)
	}
	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		DB:  *db,
		Log: *log.Default(),
	}
	s.HandleLogin(tr, req)
	if status := tr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	res := data.Login{}

	login := data.Login{
		Id:       1,
		Type:     "Login",
		Name:     "Test Login",
		Folder:   "Logins",
		Username: "Username",
		Password: "Password",
		URI:      []string{"www.example.com"},
		Notes:    "This is a note.",
	}

	unmarshalErr := json.Unmarshal(tr.Body.Bytes(), &res)
	if unmarshalErr != nil {
		t.Errorf(unmarshalErr.Error())
	}

	if !reflect.DeepEqual(res, login) {
		t.Errorf("handler returned unexpected body: got %+v, want %+v", res, login)
	}
}

func TestUpdateLoginHandler(t *testing.T) {
	updatedLogin := data.Login{
		Id:       1,
		Type:     "Login",
		Name:     "Test Login",
		Folder:   "Logins",
		Username: "Username",
		Password: "Password",
		URI:      []string{"www.example.com"},
		Notes:    "This login is no longer needed.",
	}
	var updatedLoginBuffer bytes.Buffer
	encodeErr := json.NewEncoder(&updatedLoginBuffer).Encode(updatedLogin)
	if encodeErr != nil {
		t.Errorf(encodeErr.Error())
	}
	req, err := http.NewRequest("PUT", "/api/login/1", &updatedLoginBuffer)
	if err != nil {
		t.Fatal(err)
	}
	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		DB:  *db,
		Log: *log.Default(),
	}
	s.HandleLogin(tr, req)
	if status := tr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	res := data.Login{}

	unmarshalErr := json.Unmarshal(tr.Body.Bytes(), &res)
	if unmarshalErr != nil {
		t.Errorf(unmarshalErr.Error())
	}

	if !reflect.DeepEqual(res, updatedLogin) {
		t.Errorf("handler returned unexpected body: got %+v, want %+v", res, updatedLogin)
	}

}

func TestCreateLoginHandler(t *testing.T) {
	form := url.Values{}
	form.Add("type", "Login")
	form.Add("name", "Added Test Login")
	form.Add("folder", "Logins")
	form.Add("username", "AddLoginsUsername")
	form.Add("password", "AddLoginsPassword")
	form.Add("uri", "www.example.com")
	form.Add("notes", "This login is added by the test handler.")
	req, err := http.NewRequest("POST", "/api/login/", strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		t.Fatal(err)
	}

	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		DB:  *db,
		Log: *log.Default(),
	}
	s.HandleLogin(tr, req)
	if status := tr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
	}

}
