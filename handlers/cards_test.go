package handlers

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"passward-backend/data"
	"passward-backend/database"
	"strings"
	"reflect"
	"testing"
)

func TestGetAllCardsHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/cards", nil)
	if err != nil {
		t.Fatal(err)
	}
	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		DB:  *db,
		Log: *log.Default(),
	}
	s.GetAllCards(tr, req)
	if status := tr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	res := []data.Card{}
	empty := []data.Card{}

	mastercardNotes := "For my night moves."
	mastercard := data.Card{
		Id:               1,
		Type:             "Card",
		Name:             "Bob's Mastercard",
		Cardholder_name:  "Bob Saget",
		Brand:            "Mastercard",
		Card_number:      "5296196033629922",
		Expiration_month: 10,
		Expiration_year:  2025,
		Security_code:    "679",
		Notes:            &mastercardNotes,
	}

	visaNotes := "Not needed anymore."
	visa := data.Card{
		Id:               2,
		Type:             "Card",
		Name:             "Mike's Visa",
		Cardholder_name:  "Mikel Jackson",
		Brand:            "Visa",
		Card_number:      "4716268418001665",
		Expiration_month: 11,
		Expiration_year:  2027,
		Security_code:    "103",
		Notes:            &visaNotes,
	}

	amexNotes := "For when I move west."
	amex := data.Card{
		Id:               3,
		Type:             "Card",
		Name:             "Kanye's AMEX",
		Cardholder_name:  "Kanye East",
		Brand:            "American Express",
		Card_number:      "372853200848721",
		Expiration_month: 10,
		Expiration_year:  2022,
		Security_code:    "559",
		Notes:            &amexNotes,
	}

	discoverNotes := "Need to change over to Visa."
	discover := data.Card{
		Id:               4,
		Type:             "Card",
		Name:             "Old CC",
		Cardholder_name:  "Nik Boyer",
		Brand:            "Discover",
		Card_number:      "6011032139068024",
		Expiration_month: 5,
		Expiration_year:  2022,
		Security_code:    "321",
		Notes:            &discoverNotes,
	}

	expected := append(empty, mastercard, visa, amex, discover)

	unmarshalErr := json.Unmarshal(tr.Body.Bytes(), &res)
	if unmarshalErr != nil {
		t.Errorf(unmarshalErr.Error())
	}

	if len(expected) < len(res) || len(expected) > len(res) {
		t.Errorf("handler returned unexpected body, got %+v want %+v", len(res), len(expected))
	}

	for k := range res {
		if !reflect.DeepEqual(res[k], expected[k]) {
			t.Errorf("handler returned unexpected body: got %+v, want %+v", res[k], expected[k])
		}
	}
}

func TestGetSingleCardHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/card/1", nil)
	if err != nil {
		t.Fatal(err)
	}
	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		DB:  *db,
		Log: *log.Default(),
	}
	s.HandleCard(tr, req)
	if status := tr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	res := data.Card{}

	mastercardNotes := "For my night moves."
	mastercard := data.Card{
		Id:               1,
		Type:             "Card",
		Name:             "Bob's Mastercard",
		Cardholder_name:  "Bob Saget",
		Brand:            "Mastercard",
		Card_number:      "5296196033629922",
		Expiration_month: 10,
		Expiration_year:  2025,
		Security_code:    "679",
		Notes:            &mastercardNotes,
	}

	unmarshalErr := json.Unmarshal(tr.Body.Bytes(), &res)
	if unmarshalErr != nil {
		t.Errorf(unmarshalErr.Error())
	}

	if !reflect.DeepEqual(res, mastercard) {
		t.Errorf("handler returned unexpected body: got %+v, want %+v", res, mastercard)
	}
}

func TestUpdateCardHandler(t *testing.T) {
	updatedCardNotes := "No longer needed."
	updatedCard := data.Card{
		Id:               1,
		Type:             "Card",
		Name:             "Bob's Mastercard",
		Cardholder_name:  "Bob Saget",
		Brand:            "Mastercard",
		Card_number:      "5296196033629922",
		Expiration_month: 10,
		Expiration_year:  2025,
		Security_code:    "679",
		Notes:            &updatedCardNotes,
	}
	var updatedCardBuffer bytes.Buffer
	encodeErr := json.NewEncoder(&updatedCardBuffer).Encode(updatedCard)
	if encodeErr != nil {
		t.Errorf(encodeErr.Error())
	}
	req, err := http.NewRequest("PUT", "/api/card/0", &updatedCardBuffer)
	if err != nil {
		t.Fatal(err)
	}
	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		DB:  *db,
		Log: *log.Default(),
	}
	s.HandleCard(tr, req)
	if status := tr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	res := data.Card{}

	unmarshalErr := json.Unmarshal(tr.Body.Bytes(), &res)
	if unmarshalErr != nil {
		t.Errorf(unmarshalErr.Error())
	}

	if !reflect.DeepEqual(res, updatedCard) {
		t.Errorf("handler returned unexpected body: got %+v, want %+v", res, updatedCard)
	}

}

func TestCreateCardHandler(t *testing.T) {
	form := url.Values{}
	form.Add("type", "Card")
	form.Add("name", "Test Mastercard")
	form.Add("cardholder_name", "Mastercard Tester")
	form.Add("brand", "Mastercard")
	form.Add("card_number", "5196196033629922")
	form.Add("expiration_month", "10")
	form.Add("expiration_year",  "2025")
	form.Add("security_code", "659")
	form.Add("notes", "This card is added by the test handler.")
	req, err := http.NewRequest("POST", "/api/card/", strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		t.Fatal(err)
	}

	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		DB:  *db,
		Log: *log.Default(),
	}
	s.HandleCard(tr, req)
	if status := tr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
	}

}