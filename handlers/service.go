package handlers

import (
	"log"
	"passward-backend/database"
)

type Service struct {
	Log log.Logger
	DB  database.DB
}
