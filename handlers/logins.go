package handlers

import (
	"encoding/json"
	"net/http"
	"passward-backend/data"
	"strings"

	"github.com/lib/pq"
)

func (s *Service) GetAllLogins(w http.ResponseWriter, r *http.Request) {
	login := data.Login{}
	logins := []data.Login{}
	selectAllLogins := `SELECT * FROM logins;`
	rows, rowsErr := s.DB.Query(selectAllLogins)
	if rowsErr != nil {
		s.Log.Printf("Error querying from database: %+v", rowsErr)
	}
	defer rows.Close()
	for rows.Next() {
		scanErr := rows.Scan(&login.Id, &login.Type, &login.Name, &login.Folder, &login.Username, &login.Password, pq.Array(&login.URI), &login.Notes)
		if scanErr != nil {
			s.Log.Printf("Error scanning database tables into struct: %+v", scanErr)
		}
		logins = append(logins, login)
	}
	json.NewEncoder(w).Encode(logins)
}

func (s *Service) HandleLogin(w http.ResponseWriter, r *http.Request) {
	id := strings.TrimPrefix(r.URL.Path, "/api/login/")
	switch r.Method {
	case http.MethodGet:
		login := data.Login{}

		selectSingleLogin := `SELECT * FROM logins WHERE id=$1;`
		s.Log.Printf("Getting login with ID of %+v", id)

		scanErr := s.DB.QueryRow(selectSingleLogin, id).Scan(&login.Id, &login.Type, &login.Name, &login.Folder, &login.Username, &login.Password, pq.Array(&login.URI), &login.Notes)
		if scanErr != nil {
			s.Log.Printf("Error querying from database: %+v", scanErr)
		}
		l, _ := json.Marshal(&login)
		s.Log.Printf("Returned login with ID of %+v: %v", id, string(l))
		json.NewEncoder(w).Encode(login)
	case http.MethodPost:
		r.ParseForm()
		s.Log.Printf("Creating login with values: %+v", r.Form)
		insertLogin := "INSERT INTO logins (type, name, folder, username, password, uri, notes) VALUES ($1, $2, $3, $4, $5, $6, $7)"
		_, execErr := s.DB.Exec(insertLogin, r.FormValue("type"), r.FormValue("name"), r.FormValue("folder"), r.FormValue("username"), r.FormValue("password"), pq.StringArray{r.FormValue("uri")}, r.FormValue("notes"))
		if execErr != nil {
			s.Log.Fatalf("Error happened on SQL statement execution. Error: %s", execErr)
		}
		resp := make(map[string]string)
		resp["message"] = "New login information added."
		marshaledResponse, marshalErr := json.Marshal(resp)
		if marshalErr != nil {
			s.Log.Fatalf("Error happened in JSON marshal. Err: %s", marshalErr)
		}
		s.Log.Printf("New login with values %+v created.", r.Form)
		w.WriteHeader(http.StatusCreated)
		w.Write(marshaledResponse)
	case http.MethodPut:
		updatedLogin := data.Login{}
		err := json.NewDecoder(r.Body).Decode(&updatedLogin)
		if err != nil {
			s.Log.Printf(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		UpdateLogin := `UPDATE logins SET name=$1, folder=$2, username=$3, password=$4, uri=$5, notes=$6 WHERE id=$7;`
		_, updateErr := s.DB.Exec(UpdateLogin, updatedLogin.Name, updatedLogin.Folder, updatedLogin.Username, updatedLogin.Password, pq.Array(updatedLogin.URI), updatedLogin.Notes, id)
		s.Log.Printf("Updating login with ID of %+v", id)
		if updateErr != nil {
			s.Log.Printf(updateErr.Error())
			http.Error(w, updateErr.Error(), http.StatusBadRequest)
		}
		json.NewEncoder(w).Encode(updatedLogin)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}
