package handlers

import (
	"encoding/json"
	"net/http"
	"passward-backend/data"
	"strings"
)

func (s *Service) GetAllCards(w http.ResponseWriter, r *http.Request) {
	card := data.Card{}
	cards := []data.Card{}

	selectAllCards := `SELECT * FROM cards;`

	rows, rowsErr := s.DB.Query(selectAllCards)
	if rowsErr != nil {
		s.Log.Printf("Error querying from database: %+v", rowsErr)
	}
	defer rows.Close()
	for rows.Next() {
		scanErr := rows.Scan(&card.Id, &card.Type, &card.Name, &card.Folder, &card.Cardholder_name, &card.Brand, &card.Card_number, &card.Expiration_month, &card.Expiration_year, &card.Security_code, &card.Notes)
		if scanErr != nil {
			s.Log.Printf("Error scanning database tables into struct: %+v", scanErr)
		}
		cards = append(cards, card)
	}
	json.NewEncoder(w).Encode(cards)
}

func (s *Service) HandleCard(w http.ResponseWriter, r *http.Request) {
	id := strings.TrimPrefix(r.URL.Path, "/api/card/")
	switch r.Method {
	case http.MethodGet:
		card := data.Card{}

		selectSingleCard := `SELECT * FROM cards WHERE id=$1;`
		s.Log.Printf("Getting card with ID of %+v", id)

		scanErr := s.DB.QueryRow(selectSingleCard, id).Scan(&card.Id, &card.Type, &card.Name, &card.Folder, &card.Cardholder_name, &card.Brand, &card.Card_number, &card.Expiration_month, &card.Expiration_year, &card.Security_code, &card.Notes)
		if scanErr != nil {
			s.Log.Printf("Error querying from database: %+v", scanErr)
		}
		c, _ := json.Marshal(&card)
		s.Log.Printf("Returned card with ID of %+v: %v", id, string(c))
		json.NewEncoder(w).Encode(card)
	case http.MethodPost:
		r.ParseForm()
		s.Log.Printf("Creating card with values: %+v", r.Form)
		insertCard := "INSERT INTO cards (type, name, folder, cardholder_name, brand, card_number, expiration_month, expiration_year, security_code, notes) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)"
		_, execErr := s.DB.Exec(insertCard, r.FormValue("type"), r.FormValue("name"), r.FormValue("folder"), r.FormValue("cardholder_name"), r.FormValue("brand"), r.FormValue("card_number"), r.FormValue("expiration_month"), r.FormValue("expiration_year"), r.FormValue("security_code"), r.FormValue("notes"))
		if execErr != nil {
			s.Log.Fatalf("Error happened on SQL statement execution. Error: %s", execErr)
		}
		resp := make(map[string]string)
		resp["message"] = "New card information added."
		marshaledResponse, marshalErr := json.Marshal(resp)
		if marshalErr != nil {
			s.Log.Fatalf("Error happened in JSON marshal. Err: %s", marshalErr)
		}
		s.Log.Printf("New card with values %+v created.", r.Form)
		w.WriteHeader(http.StatusCreated)
		w.Write(marshaledResponse)
	case http.MethodPut:
		updatedCard := data.Card{}
		err := json.NewDecoder(r.Body).Decode(&updatedCard)
		if err != nil {
			s.Log.Printf(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		UpdateCard := `UPDATE cards SET type=$1, name=$2, folder=$3, cardholder_name=$4, brand=$5, card_number=$6, expiration_month=$7, expiration_year=$8, security_code=$9, notes=$10 WHERE id=$11;`
		_, updateErr := s.DB.Exec(UpdateCard, updatedCard.Type, updatedCard.Name, updatedCard.Folder, updatedCard.Cardholder_name, updatedCard.Brand, updatedCard.Card_number, updatedCard.Expiration_month, updatedCard.Expiration_year, updatedCard.Security_code, updatedCard.Notes, id)
		s.Log.Printf("Updating card with ID of %+v", id)
		if updateErr != nil {
			s.Log.Printf(updateErr.Error())
			http.Error(w, updateErr.Error(), http.StatusBadRequest)
		}
		json.NewEncoder(w).Encode(updatedCard)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}
