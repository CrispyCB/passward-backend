package main

import (
	"log"
	"net/http"
	"os"
	"passward-backend/database"
	"passward-backend/handlers"
	"time"

	"github.com/rs/cors"
)

func main() {
	l := log.Default()

	passwardMux := http.NewServeMux()

	db, dbErr := database.SetupDatabase()
	if dbErr != nil {
		l.Printf("Error connecting to database: %+v", dbErr)
	}

	passwardService := handlers.Service{
		DB:  *db,
		Log: *l,
	}

	passwardMux.HandleFunc("/api/logins", passwardService.GetAllLogins)
	passwardMux.HandleFunc("/api/login/", passwardService.HandleLogin)
	passwardMux.HandleFunc("/api/cards", passwardService.GetAllCards)
	passwardMux.HandleFunc("/api/card/", passwardService.HandleCard)

	Addr := os.Getenv("ADDR")
	c := cors.Default().Handler(passwardMux)
	s := http.Server{
		Addr:         Addr,
		Handler:      c,
		ErrorLog:     l,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	l.Printf("Starting server on port %+v", Addr)
	SvErr := s.ListenAndServe()
	// if error, handle
	if SvErr != nil {
		l.Printf("Error starting server: %+v", SvErr)
		os.Exit(1)
	}

}
