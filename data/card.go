package data

type Card struct {
	Id   int	`json:"id"`
	Type string	`json:"type"`
	Name string	`json:"name"`
	// Folder is a pointer to type string because if there is no "Folder" value in the returned DB call, Folder will be nil.
	Folder           *string	`json:"folder"`
	Cardholder_name  string	`json:"cardholder_name"`
	Brand            string	`json:"brand"`
	Card_number      string	`json:"card_number"`
	Expiration_month int	`json:"expiration_month"`
	Expiration_year  int	`json:"expiration_year"`
	Security_code    string	`json:"security_code"`
	// Same logic for Notes as for Folder.
	Notes *string	`json:"notes"`
}
