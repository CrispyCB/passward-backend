package data

type Login struct {
	Id       int      `json:"id"`
	Type     string   `json:"type"`
	Name     string   `json:"name"`
	Folder   string   `json:"folder"`
	Username string   `json:"username"`
	Password string   `json:"password"`
	URI      []string `json:"uri"`
	Notes    string   `json:"notes"`
}
